* Included Thread.sleep() in producer to simulate irregular feed
* Developed on a 32-bit PC hence no Docker
* If null returned to Producer from post then stop/delay production as Consumer has a problem
* Consumer uses Jersey framework
* Created DB eventrepo in Mongo
* Probably want to separate ccy and amount in transaction amount
* Solution requires db eventrepo and collection events to exist in Mongo
* Could not find a solution to include space in Mongo database name so condensed event repo to eventrepo